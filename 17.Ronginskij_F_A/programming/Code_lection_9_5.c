#include<stdio.h>

int sum_of_divs(int);

int main()
{
  int i, limit, jSum, iSum;
  char fileName[20];
  FILE *myFile;
  printf("Enter filename: ");
  scanf("%s", fileName);
  if((myFile = fopen(fileName, "w")) == NULL){
    printf("Error!");
    return -1;
  }
  printf("Enter limit: ");
  scanf("%d", &limit);
  for(i = 2; i < limit; i++){
    iSum = sum_of_divs(i);
    if(iSum < i)
     continue;
    jSum = sum_of_divs(iSum);
    if(i == jSum && iSum != 0)
      fprintf(myFile, "%d : %d\n", iSum, jSum);
      }
  fclose(myFile);
  return 0;
}

int sum_of_divs(int number)
{
  int i;
  int sum = 0;
  int n = number/2;
  for(i = 1; i <= n; i++){
    if(number%i == 0)
      sum += i;
  }
  return sum;
}
