#include<stdio.h>

#define SIZE 1

int sym(int[],int);

int main()
{
  int a[SIZE] = {1};
  if(sym(a,SIZE))
    printf("Sym!\n");
  else
    printf("Not sym!\n");
  return 0;
}

int sym(int arr[], int size)
{
  int end = size-1;
  int start = 0;
  while(start < end){
    if(arr[start] != arr[end])
      return 0;
    ++start;
    --end;
  }
  return 1;
}
