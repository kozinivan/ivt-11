#include<stdio.h>

#define FILE_NAME_SIZE 30
#define LINE_SIZE 100

int main()
{
  char fileName[FILE_NAME_SIZE];
  char line[LINE_SIZE];
  int nlines = 1;
  char newFileName[] = "temp.123123";
  FILE *myfile;
  FILE *newfile;
  printf("Enter file name: ");
  scanf("%s",fileName);
  if((myfile = fopen(fileName,"r"))  == NULL){
    printf("Failde to open file %s\n", fileName);
  }
  else if((newfile = fopen(newFileName,"w")) == NULL){
    printf("Failed to open file %s\n", newFileName);
  }
  else{
    while((fgets(line, LINE_SIZE-1, myfile)) != NULL){
      if(nlines != 1)
	fprintf(newfile, "%s", line);
      ++nlines;
    }
    fclose(myfile);
    fclose(newfile);
    rename("temp.123123",fileName);
  }
  return 0;
}
