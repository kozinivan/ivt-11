#include<stdio.h>
#include<stdlib.h>
#define SIZE 10
void bubble(int *, int);
void output(int *);
int main()
{
  srand(time(NULL));
  int array[SIZE];
  for (int i = 0; i < SIZE; i++)
  {
    array[i] = (rand() % 100) + 1;
  }
  output(array);
  bubble(array, SIZE);
  output(array);
  return 0;
}
void output(int *a)
{
  for (int i = 0; i < SIZE; i++)
  {
    printf("a[%d] = %d\n", i, *(a+i));
  }
  printf("\n");
}
void bubble(int *a, int S)
{
  int j, i, buf, c;
  for (j = S-1; j > 0; j--)
  {
    c = 0;
    for (i = 1; i <= j; i++)
    {
      if (*(a+i) > *(a+i-1))
	{
	  c = 1;
	  buf = *(a+i-1);
	  *(a+i-1) = *(a+i);
	  *(a+i) = buf;
	}
    }
    if (c == 0)
      break;
  }
}
