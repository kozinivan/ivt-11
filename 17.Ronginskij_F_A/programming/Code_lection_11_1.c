#include <stdio.h>
#include <ctype.h>

int main()
{
  char   c[10];
  printf("Введите строку: ");
  scanf("%s", c);
  int i = 0;
  while (c[i] != '\0')
    {
      if (islower(c[i]))
	c[i] = toupper(c[i]);
      else 
	c[i] = tolower(c[i]);
      i++;
    }
  printf ("Новая строка: %s\n", c);
  return 0;
}

