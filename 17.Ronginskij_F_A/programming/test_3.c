#include<stdio.h>
#include<math.h>
#define pi 3.14
float fun(float,float);
int main()
{
float x,y;
for(x=-1*pi;x<=pi;x+=pi/4){
printf("\n");
for(y=-2*pi;y<=2*pi;y+=pi/4)
{
if(x==0.0 && y<0.001 && y>-0.001)
{
printf("Значение функции неизвестно\n");
continue;
}
printf("%.lf  %.lf %.3f \n",x,y,fun(x,y));
}
}
return 0;
}	
float fun(float x,float y)
{
return (x*cos(x)+y*cos(y))/(pow(x,2)-pow(y,2));
}
