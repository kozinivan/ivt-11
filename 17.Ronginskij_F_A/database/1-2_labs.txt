Create database Base6 COLLATE='utf8_unicode_ci';
use Base6;
DROP TABLE IF EXISTS `�������� �������`;
DROP TABLE IF EXISTS `�����`;
DROP TABLE IF EXISTS `������������`;
DROP TABLE IF EXISTS `��������� �������`;
DROP TABLE IF EXISTS `�������`;
DROP TABLE IF EXISTS `����� ���������`;
DROP TABLE IF EXISTS `���������`;
DROP TABLE IF EXISTS `����� �������`;
DROP TABLE IF EXISTS `����� �����`;
DROP TABLE IF EXISTS `�����`;

  

CREATE TABLE `�������` (
  `�������` VARCHAR(17) NOT NULL,
  `������������` VARCHAR(62) NULL,
  `������` INT NOT NULL,
  `�����` INT NULL,
  `�����������` VARCHAR(17) NULL,
  `�����������` TEXT NULL,
  PRIMARY KEY (`�������`) 
  );
  CREATE TABLE `������������` (
  `�����` varchar(32) NOT NULL,
  `������` varchar(32) NOT NULL,
  `����` ENUM('����','����'),
  `������������` INT NULL,
   ID INT UNIQUE KEY NOT NULL,
  PRIMARY KEY (`�����`,`������`)
  );
  CREATE TABLE `�����` (
  `�����` INT NOT NULL,
  `����` DATE NOT NULL,
  `���� ����������` VARCHAR(15) NOT NULL,
  `��������` INT NOT NULL,
  `��������` INT NULL,
  `���������` INT NULL,
  PRIMARY KEY (`�����`,`����`),
  FOREIGN KEY (`��������`) REFERENCES `������������` (ID) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY (`��������`) REFERENCES `������������` (ID) ON DELETE NO ACTION ON UPDATE CASCADE
  );
CREATE TABLE `�������� �������` (
  `����� ������` INT NOT NULL,
  `������� �������` VARCHAR(17) NOT NULL,
  `����������`  INT NOT NULL,
  PRIMARY KEY (`����� ������`,`������� �������`),
    FOREIGN KEY (`����� ������`) REFERENCES `�����`(`�����`) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (`������� �������`) REFERENCES `�������`(`�������`) ON DELETE NO ACTION ON UPDATE CASCADE
  );

CREATE TABLE `�����` (
	`�������` VARCHAR(15) NOT NULL,
	`������������` VARCHAR(60) NULL,
	`����` VARCHAR(40) NULL,
	`�������` VARCHAR(30) NULL,
	`�����������` VARCHAR(15) NULL,
	`������` VARCHAR(40) NULL,
	`������` INT NULL,	  
	`�����` INT NULL,
	`����`  INT NULL,
    PRIMARY KEY (`�������`)
);
 CREATE TABLE `����� �����` (
  `�����` INT NOT NULL,
  `������` INT NOT NULL,
  `�����` INT NOT NULL,
  `������� �����` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`�����`,`������� �����`),
	FOREIGN KEY (`������� �����`) REFERENCES `�����`(`�������`) ON DELETE NO ACTION ON UPDATE CASCADE
  );
  CREATE TABLE `����� �������` (
  `������� �����` VARCHAR(15) NOT NULL,
  `������� �������` VARCHAR(17) NOT NULL,
  PRIMARY KEY (`������� �����`,`������� �������`),
  FOREIGN KEY (`������� �����`) REFERENCES `�����`(`�������`) ON DELETE NO ACTION ON UPDATE CASCADE
  );
  

  
   CREATE TABLE `���������` (
  `�������` varchar(25) NOT NULL,
  `������������` VARCHAR(30) NOT NULL,
  `���` VARCHAR(15) NOT NULL,
  `������` INT NOT NULL,
  `�����` INT NULL,
  `���` INT NULL,
  `�����������` VARCHAR(25) NULL,
  `����` INT NOT NULL,
   PRIMARY KEY (`�������`)
  );

CREATE TABLE `��������� �������` (
  `������� ���������` VARCHAR(25) NOT NULL,
  `������� �������` VARCHAR(17) NOT NULL,
  `����������` VARCHAR(40) NOT NULL,
  `������` INT NULL,
  `�����` INT NULL,
  `�������` INT NULL,
  `����������` int NOT NULL,
  PRIMARY KEY (`������� ���������`,`������� �������`),
  FOREIGN KEY (`������� �������`) REFERENCES  `�������` (`�������`) ON DELETE NO ACTION ON UPDATE CASCADE,
  FOREIGN KEY (`������� �������`) REFERENCES  `���������` (`�������`) ON DELETE NO ACTION ON UPDATE CASCADE
  );
   CREATE TABLE `����� ���������` (
  `������` INT NOT NULL,
  `������� ���������` varchar(25) NOT NULL,
  `����������` INT NOT NULL,
  PRIMARY KEY (`������`,`������� ���������`),
  FOREIGN KEY (`������� ���������`) REFERENCES `���������`(`�������`) ON DELETE NO ACTION ON UPDATE CASCADE
  );


LOAD DATA INFILE "C:\\base3.csv"
INTO TABLE `�����`
COLUMNS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;
select * from `�����`;