#include <stdio.h>
#include <math.h>
#include <stdlib.h>
    float f(float x,float c,float d) //возвращает значение функции
    {
	  return exp(c*x)-d; // Значение функции 
	}
     float f1(float x,float c)
     { 
      return c*exp(c*x);// Значение производной функции 
     }
     float f2(float x,float c)
     {
	return pow(c,2)*exp(c*x); // Значение второй производной функции 
	 }
 
float Newton(float a,float b,float (*f)(float),float (*f1)(float),float (*f2)(float),float eps) //функция метода касательных(Ньютона)
{
      float xn,x0,m=0; //m-это переменная для цикла
      if(f(a)*f2(a)>0) //для выбора начальной точки проверяем f(x0)*f2(x0)>0 
		  x0=a;
         else  
		  x0=b;
      do
        {
               x0=xn;
               xn=x0-f(x0)/f1(x0); //формула Ньютона
        }
        while(fabs(xn-x0)>eps); //пока не достигнем необходимой точности, будет продолжать вычислять
        { 
		m++; 
		}
        return x0;
        }
 
int main ()
 
{
     float x0,xn; //вычисляемые приближения для корня
	 float a,b,eps,c,d,x;// границы отрезка,параметры и необходимая точность
     printf("Введите a, b, eps: ");//с помощью a и b задаем границу отрезка,а с помощью eps задаем точность вычислений 
     scanf("%f%f%f", &a, &b, &eps);
     printf("Введите x,c и d:");
	 scanf("%f%f%f", &x, &c, &d);
     f(x,c,d);
	 f1(x,c);
	 f2(x,c);
     xn=Newton(a,b,f,f1,f2,eps);
     printf("Корень: x=%1.3f\n" ,xn); //вывод корня
     return 0;
     }