#include <stdio.h>//стандартный заголовочный файл ввода-вывода 
#include <math.h> //подключаем математическую библиотеку для дальнейшнего использования функции
#define eps 0.000000001 //задаём точность вычислений
typedef double (*func)(double x, double c, double d); // задаем тип func
//Объявим прототипы функций
double Newton(func,func,double,double,double); // прототип нахождения корня функции
double fx(double, double, double); // прототип вычисляемой функции
double dfx(double, double, double); // прототип производной функции

double fx(double x, double c, double d) // вычисляемая функция
{
  return pow(x,5)+c*pow(x,2)-d; //возвращаем значение функции
}

double dfx(double x, double c, double d) // производная функции
{
  return 5*pow(x,4)+2*c*x; //возвращаем значение производной
}

double Newton(func fx, func dfx, double a, double c, double d) //нахождение корня
{
  double b; //доп-я переменная для вычисления
  b = a - fx(a, c, d) / dfx(a, c, d); // первое приближение
  while (fabs(b - a) > eps)// вычисляем,пока не достигнута точность eps(0.000000001)
  {
    a = b;
    b = b - fx(b, c, d) / dfx(b, c, d); // последующие приближения
  }
  return b; //возвращаем значение b
}


int main()
{
  double a,c,d; //задаём параметры  
  printf("Введите c и d:"); 
  scanf("%lf%lf", &c, &d);
  printf("Введите a: ");
  scanf("%lf", &a);
  printf("Ответ : %lf\n", Newton(fx, dfx, a, c, d)); // Вывод ответа
  return 0;
}