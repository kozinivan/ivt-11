//подключаем библиотеки
#include<stdio.h> 
#include<stdlib.h> 
#include<math.h> //подключаем математическую библиотеку для дальнейшего вычисления выражения
#define eps 0.000000001
int main() 
{ 
double x; //вводим необходимые параметры
printf("Введите x:"); 
scanf("%lf",&x); 
double xn;//тут храним N-ый член ряда 
double s = 0;//тут храним значение суммы 
int count = 0;//начальное значение count
xn = x;//начальное значение xn 
if ((x <= -1) || (x >= 1)) //проверяем значение x 
{ 
printf("Неверные данные!"); 
} 
else 
{ 
while (fabs(xn) > eps) 
{ 
s += xn;//суммируем член ряда 
count++; //переходим к след.члену
xn = pow(x, 4 * count + 1) / (4 * count + 1); 
printf("%lf\n членов ряда:%d\n",s,count); 
} 
} 
return s;//Получившаяся сумма 
}

