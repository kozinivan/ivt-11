#include <stdio.h> //стандартный заголовочный файл ввода-вывода
#include <math.h> //подключаем математическую библиотеку для исп-я в дальнейшем функции
#define eps 0.000000001 //задаем точность вычисления
typedef double (*func)(double x, double c, double d); // задаем тип func
double fx(double, double, double);      // прототип вычисляемой функции
double fx(double x, double d, double c) // вычисляемая функция
{
  return exp(c*x)-d; 
}

double root(func fx, double a, double b, double c, double d) //нахождение корня
{
  double f; //доп-я переменная длы вычисления
  while (fabs(a - b) > eps) // пока не достигнем точности
  {
    f = (a * fx(b, c, d) - b * fx(a, c, d)) / (fx(b, c, d) - fx(a, c, d));
    a = b;
    b = f;
  }
  return a; //возвращаем значение a
}

int main()
{
  double c,d,a,b; //задаём параметры и границы отрезка,где a, b - начало и конец отрезка
  printf("Введите c и d:"); 
  scanf("%lf%lf", &c, &d);
  printf("Введите a, b: ");
  scanf("%lf%lf", &a, &b); 
  printf("Ответ:%.8f\n", root(fx, a, b, d, c)); // Вывод ответа
  return 0;
}