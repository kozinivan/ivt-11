#! /bin/bash

# ssh $1
if [ $# -eq 0 ]; then
  echo "Usage: $0 path_to_source path_to_destiantion"
  exit 0
fi
if [ ! -d $2 ]; then
  echo "Error: Source directory does not exist!"
  exit 0
fi
if [ ! -d $3 ]; then
  mkdir $3
fi
cp -r $2/. $3/
find $3 -type f -exec chmod a=r {} \;
find $3 -type d -exec chmod a=rx {} \;
exit 0
