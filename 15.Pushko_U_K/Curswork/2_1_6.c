﻿#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{ int n = 0;
double x, a, b, y1, y2, EPS,e,c,d;
printf("Введите промежуток a и b: "); scanf("%lf%lf",&a,&b);
float a1=a, b1=b;
printf("Введите c и d: "); scanf("%lf%lf",&c,&d);
printf("Введите точность(кол-во знаков после запятой)"); scanf("%lf",&EPS); EPS=powf(10,-EPS);//Задаем точность
y1=a*a*a*a*a+c*a*a-d;
y2=b*b*b*b*b+c*b*b-d;
if (y1*y2>0) printf("Корней нет");
else{
do { ++n;
x = (a+b)/2;
y1 = a*a*a*a*a+c*a*a-d;
y2 = x*x*x*x*x+c*x*x-d;
if (y1*y2>0) a = x;
else b = x;
} while ((b-a)>EPS);
x = (a + b)/2;
printf("Корень уравнения на отрезке %f, %f равен %lf и получен за %d шагов",a1,b1,x,n);}
return 0; }

