﻿#include <math.h>//подключение библеотек
#include <stdio.h>
#include <stdlib.h>
#define M_PI       3.141592// объявление числа Pi
double Resh(double,double,double);// объявление функции
double Resh(double EPS, double x,double z)//функция
{ 
    int n=1;double S=0;//ввод локальных переменных
	while(fabs(S-z)>=EPS)//цикл цепного ряда
	{
	S+=cos((n)*x)/pow((n),2);n+=2;//увеличивающиясе выражение цепного ряда
	}
	printf("Понадобилось членов ряда:%d\n",n/2);//вывод числового ряда
	return S;//возвращение значения в main
	}
int main()
{
	double EPS,z,x;//ввод переменных 
	printf("Введите значение x (|x|<0):"); scanf("%lf",&x);//задаем значение x
	printf("Введите точность(кол-во знаков после запятой)"); scanf("%lf",&EPS); EPS=powf(10,-EPS);//Задаем точность
	z=pow(M_PI,2)/8-(M_PI/4*fabs(x));//значение выражения
	printf("Сравнение: %lf\t%lf\n",z,Resh(EPS,x,z));//Сравнение результатов
	return 0;//конец программы
}