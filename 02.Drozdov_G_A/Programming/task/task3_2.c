#include <stdio.h>
#include <math.h>
float func(float,float);
int main()
{
    float x,y;
    for(x=0;x<=0.5;x+=0.1) {
        printf("\n");
        for(y=-1;y<=1;y+=0.2) 
            {
                if(x == 0.0 && y<0.001 && y>-0.001) 
                {
                    printf("При х=0 и y=0 значение функции неизвестно. \n ");
                    continue;
                }
                printf("f(x,y) ,при x=%.1f и y=%.1f, равно %.3f \n ",x,y,func(x,y));
            } 
    }
    return 0;
}
float func (float x , float y){
return (sin(x)+sin(y))/(pow(x,2)+pow(y,2));
}