#include <stdio.h>

int main() {
	int x, y;
	int coord = 0;

	printf("Введите X и Y:\n");

	printf("X = "); scanf("%d", &x);
	printf("Y = "); scanf("%d", &y);

	if (x > 0 && y > 0) coord = 1;
	if (x < 0 && y > 0) coord = 2;
	if (x < 0 && y < 0) coord = 3;
	if (x > 0 && y < 0) coord = 4;

	printf("Точка XY находится в %d координатной четверти.\n", coord);
	
	return 0;
}
