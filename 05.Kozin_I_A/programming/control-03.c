#include <stdio.h>
#include <math.h>

float function(double, double);
float function(double x, double y) { return (exp(x) + exp(x * (-1))) / (exp(y) - exp(y * (-1))) * asin((x + y) / 2); }

int main() {
	double x, y;

	for (x = 0.0; x <= 2.0; x += 0.25) {
		for (y = -0.5; y <= 0.5; y += 0.1) {
			printf("f(%.3f, %.3f) = %.3lf\n", x, y, function(x, y));
		}
	}
	
	return 0;
}
