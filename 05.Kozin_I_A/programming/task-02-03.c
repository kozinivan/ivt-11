#include <stdio.h>

int main() {
	int a, b, c, d, e; // Ввод переменных
	int x = 0, y = 0; // x - Наибольшее; y - наименьшее
	int z = 0; // Сумма наибольшего и наименьшего

	printf("Введите 5 чисел:\n");

	printf("a = "); scanf("%d", &a);
	printf("b = "); scanf("%d", &b);
	printf("c = "); scanf("%d", &c);
	printf("d = "); scanf("%d", &d);
	printf("e = "); scanf("%d", &e);

	if (a > b && a > c && a > d && a > e) { x = a; } else if (b > a && b > c && b > d && b > e) { x = b; } else if (c > a && c > b && c > d && c > e) { x = c; } else if (d > a && d > b && d > c && d > e) { x = d; } else if (e > a && e > b && e > c && e > d) { x = e; }
	if (a < b && a < c && a < d && a < e) { y = a; } else if (b < a && b < c && b < d && b < e) { y = b; } else if (c < a && c < b && c < d && c < e) { y = c; } else if (d < a && d < b && d < c && d < e) { y = d; } else if (e < a && e < b && e < c && e < d) { y = e; }

	z = x + y;

	printf("Наибольшее число: %d\n", x);
	printf("Наименьшее число: %d\n", y);
	printf("Сумма наибольшего и наименьшего чисел: %d\n", z);

	return 0;
}
