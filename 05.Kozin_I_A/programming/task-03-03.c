#include <stdio.h>

int main() {
	int x, i = 0;
	printf("Укажите размер квадрата: "); scanf("%d", &x);
	
	while (i <= x * x) { if (i % x == 0) printf("*\n"); else printf("*"); i++; }
	
	return 0;
}
