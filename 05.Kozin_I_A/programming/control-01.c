#include <stdio.h>
#include <math.h>

int main() {
	int a,b,c;
	double g1,g2,P,R,T;
	
	printf("Введите a: "); scanf("%d", &a);
	printf("Введите b: "); scanf("%d", &b);
	printf("Введите c: "); scanf("%d", &c);
	
	printf("Введите g1: "); scanf("%lf", &g1);
	printf("Введите g2: "); scanf("%lf", &g2);
	
	P = ((10^3) / (g1 * g2)) + (1 / (a + b));
	R = ((a * b) / c) + (b / (a * c));
	T = ((a + b) / (c + b)) + ((a + b / c) / (c + a / b));
	
	if ((g1 * g2 == 0) || (a + b == 0)) { printf("P не найден"); } else { printf("P = %lf", P); } printf("\n");
	if ((c == 0) || (a * c == 0)) { printf("R не найден"); } else { printf("R = %lf", R); } printf("\n");
	if ((c + b == 0) || (c + a / b == 0)) { printf("T не найден"); } else { print("T = %lf", T); } printf("\n");
	
	return 0;
}