#include <stdio.h>

int main() {
	int x, y = 0; // x - ввод; y - вывод
	
	printf("Введите переменную x: "); scanf("%d", &x);
	
	if (x < -2 || x > 2) { y = 2 * x; } else if (x == -2 || x == 2) { y = -3 * x; } else { y = 5 * x; }

	printf("Значение функции f(x) = %d\n", y);
	
	return 0;
}
