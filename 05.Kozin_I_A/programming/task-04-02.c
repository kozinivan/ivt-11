#include <stdio.h>
#include <stdlib.h>

int main() {
	int i, n;
	
	printf("Введите число: "); scanf("%d", &n);
	
	for (i = (n - 1); i >=1; i--) n *=i;
	
	printf("Значение факториала равно: %d\n", n);
	
	return 0;
}
