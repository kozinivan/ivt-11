#include <stdio.h>
#include <stdlib.h>

int main() {
    int x, m, k, n1, n2, n3, n4;

    printf("Введите число: "); scanf("%d", &x);
    
    system("clear");
    
    m = x;
    n1 = x / 10000;
    n2 = (x / 1000) % 10;
    n3 = (x % 100) / 10;
    n4 = x % 10;
    
    if ((n1 == n4) && (n2 == n3)) { printf("Это палиндром!\n"); } else { printf("Это не палиндром!\n"); }
    
    return 0;
}
