#include <stdio.h>

int main() {
	double x; // x - ввод
	int y = 0; // y - вывод

	printf("Введите переменную x: \n"); scanf("%lf", &x);

	if (x < 0) y = 0; else if (x <= 0 && x < 1) y = 1; else if (x <= 1 && x < 2) y = -1;

	printf("Функция f(x) равняется: %d\n", y);
	
	return 0;
}
