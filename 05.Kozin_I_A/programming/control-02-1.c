#include <stdio.h>
#include <math.h>

int main() {
	double a,b,c,x,y,z,S,P,R;
	printf("Введите сторону a: \n"); scanf("%lf", &a);
	printf("Введите прилежащий угол: \n"); scanf("%lf", &x);
	printf("Введите площадь треугольника: \n"); scanf("%lf", &S);
	
	b = 2 * S / (a * sin(x));
	c = sqrt(b^2 + a^2 - 2 * b * a * cos(x));
	P = a + b + c;
	
	y = (a^2 + c^2 - b^2) / (2 * a * c);
	z = (b^2 + c^2 - a^2) / (a * c * b);
	
	R = (a + b + c) / 2;
	R = (a * b * c) / (4 * sqrt(R * (R - a) * (R - b) * (R - c)));
	
	printf("Сторона b = %lf\n", b);
	printf("Второй угол = %lf\n", y);
	printf("Сторона c = %lf\n", c);
	printf("Третий угол = %lf\n", z);
	printf("Периметр = %lf\n", P);
	printf("Радиус описанной окружности равен = %lf", R);
	
	return 0;
}