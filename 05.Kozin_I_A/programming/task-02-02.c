#include <stdio.h>

int main() {
	int a, b, c; // Входящие переменные
	int x = 0, y = 0, z = 0; // Выходящие переменные

	printf("Введите 3 переменные:\n");

	printf("a = "); scanf("%d", &a);
	printf("b = "); scanf("%d", &b);
	printf("c = "); scanf("%d", &c);

	if (a > b && a > c) { x = a; } else if (b > a && b > c) { x = b; } else if (c > a && c > b) { x = c; }
	if (a > b && a < c) { y = a; } else if (b > a && b < c) { y = b; } else if (c > a && c < b) { y = c; }
	if (a < b && a < c) { z = a; } else if (b < a && b < c) { z = b; } else if (c < a && c < b) { z = c; }
	
	printf("Расстановка переменных в порядке возрастания:\n%d\n%d\n%d\n", x, y, z);

	return 0;
}
