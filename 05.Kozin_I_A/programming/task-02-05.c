#include <stdio.h>

int main() {
	double A, B, C;

	printf("Введите 3 переменные:\n");

	printf("A = "); scanf("%lf", &A);
	printf("B = "); scanf("%lf", &B);
	printf("C = "); scanf("%lf", &C);

	if (A > B && A > C && B > C) { // A B C
		A *= A;
		B *= B;
		C *= C;
		printf("Числа расставлены по порядку и умножены в два раза:\n");
	} else {
		A *= -1;
		B *= -1;
		C *= -1;
		printf("Числа расставлены не по порядку и заменены на противоположные:\n");
	}

	printf("A = %lf\nB = %lf\nC = %lf\n", A, B, C);
	
	return 0 ;
}
