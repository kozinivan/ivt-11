				>enable
				#configure terminal
(config)		#hostname Router1

(config)		#interface Serial0/0/0
(config-if)		#ip address 192.168.253.254 255.255.255.252
(config-if)		#no shutdown
(config-if)		#exit

(config)		#interface Serial0/1/0
(config-if)		#ip address 192.168.254.253 255.255.255.252
(config-if)		#no shutdown
(config-if)		#exit

(config)		#interface FastEthernet0/0
(config-if)		#ip address 192.168.8.1 255.255.255.0
(config-if)		#no shutdown
(config-if)		#exit

(config)		#ip dhcp pool 204h
(dhcp-config)	#network 192.168.8.0 255.255.255.0
(dhcp-config)	#default-router 192.168.8.1
(dhcp-config)	#exit
(config)		#ip dhcp excluded-address 192.168.8.1

(config)		#ip route 192.168.0.0 255.255.252.0 192.168.253.253
(config)		#ip route 192.168.4.0 255.255.252.0 192.168.253.253
(config)		#ip route 192.168.9.0 255.255.255.0 192.168.254.254
(config)		#ip route 192.168.10.0 255.255.255.224 192.168.254.254

(config)		#exit

				#copy running-config startup-config