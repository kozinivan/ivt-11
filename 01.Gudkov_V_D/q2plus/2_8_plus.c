/* Студент: Гудков Вячеслав Дмитриевич */
#include<stdio.h>

int main()
{
 int x,f;
 x = f = 0;
 printf("Enter value for x: ");
 scanf("%d",&x);
 if(x < -2 || x > 2)
   f = x * 2;
 else if(x == -2 || x == 2)
   f = x * -3;
 else
   f = x * 5;
 printf("f(%d) = %d\n", x,f);
 return 0;
}
