/* Студент: Гудков Вячеслав Дмитриевич */
#include<stdio.h>

int main()
{
 int ax,ay,bx,by,cx,cy,dx,dy;
 ax = ay = bx = by = cx = cy = dx = dy = 0;
 printf("Enter x coordinate for A point: ");
 scanf("%d",&ax);

 printf("Enter y coordinate for A point: ");
 scanf("%d",&ay);

 printf("Enter x coordinate for B point: ");
 scanf("%d",&bx);

 printf("Enter y coordinate for B point: ");
 scanf("%d",&by);

 printf("Enter x coordinate for C point: ");
 scanf("%d",&cx);

 printf("Enter y coordinate for C point: ");
 scanf("%d",&cy);

 if(ax == bx)
   dx = cx;
 else if(ax == cx)
   dx = bx;
 else
   dx = ax;

 if(ay == by)
   dy = cy;
 else if(ay == cy)
   dy = by;
 else
   dy = ay;
 printf("(%d,%d)\n", dx,dy);
 return 0;
}
