//Студент: Гудков Вячеслав Дмитриевич
#include<stdio.h>

int main()
{
  int a, current;
  a = 0;
  current = 1;
  printf("Enter size of a side: ");
  scanf("%d",&a);
  while(current <= a*a){
    putchar('*');
    if(current%a==0)
      putchar('\n');
    ++current;
  }
  return 0;
}
