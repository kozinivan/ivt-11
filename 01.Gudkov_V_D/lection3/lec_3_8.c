//Студент: Гудков Вячеслав Дмитриевич
#include<stdio.h>
#include<unistd.h>

int main()
{
  int i;
  i = 2;
  while(1){
    printf("%d ",i);
    i*=2;
    usleep(1000);
    if(!i){
      printf("int overflow\n");
      usleep(1000000);
    }
  }
  return 0;
}
