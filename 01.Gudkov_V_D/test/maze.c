#include<stdio.h>

#define HEIGHT 12
#define WIDTH 12
#define UP 2
#define RIGHT 1
#define DOWN -2
#define LEFT -1

void printMaze(char[][WIDTH], char[][WIDTH]);
int findExit(char[][WIDTH], int, int);

int main()
{
  char maze[HEIGHT][WIDTH] = {
    {'1','1','1','1','1','1','1','1','1','1','1','1'},
    {'1','0','0','0','1','0','0','0','0','0','0','1'},
    {'0','0','1','0','1','0','1','1','1','1','0','1'},
    {'1','1','1','0','1','0','0','0','0','1','0','1'},
    {'1','0','0','0','0','1','1','1','0','1','0','0'},
    {'1','1','1','1','0','1','0','1','0','1','0','1'},
    {'1','0','0','1','0','1','0','1','0','1','0','1'},
    {'1','1','0','1','0','1','0','1','0','1','0','1'},
    {'1','0','0','0','0','0','0','0','0','1','0','1'},
    {'1','1','1','1','1','1','0','1','1','1','0','1'},
    {'1','0','0','0','0','0','0','1','0','0','0','1'},
    {'1','1','1','1','1','1','1','1','1','1','1','1'}
  };
  char mazePath[HEIGHT][WIDTH] = {0};
  int wall = 0;
  int playerY = 4;
  int playerX = 4;
  int exitFound = 0;

  mazePath[playerY][playerX] = 'X';
  while(wall != UP){
    printMaze(maze,mazePath);
    if(findExit(maze,playerX,playerY) == 1){
      exitFound = 1;
      break;
    }
    if(maze[playerY-1][playerX] == '1'){
      wall = UP;
    }
    else if(maze[playerY][playerX+1] == '0'){
      ++playerX;
    }
    else if(maze[playerY+1][playerX] == '0'){
      ++playerY;
    }
    else if(maze[playerY][playerX-1] == '0'){
      --playerX;
    }
    mazePath[playerY][playerX] = 'X';
  }
  while(!exitFound){
    while(wall == UP){
      if(findExit(maze,playerX,playerY) == 1){
        exitFound = 1;
	break;
      }
      if(maze[playerY][playerX+1] == '0'){
        ++playerX;
        mazePath[playerY][playerX] = 'X';
        printMaze(maze,mazePath);
        wall = (maze[playerY-1][playerX] == '1') ? UP : LEFT;
      }
      else if(maze[playerY+1][playerX] == '1'){
        wall = DOWN;
      }
      else{
        wall = RIGHT;
      }
    }
    while(wall == LEFT){
      if(findExit(maze,playerX,playerY) == 1){
        exitFound = 1;
	break;
      }     
      if(maze[playerY-1][playerX] == '0'){
        --playerY;
        mazePath[playerY][playerX] = 'X';
        printMaze(maze,mazePath);
        wall = (maze[playerY][playerX-1] == '1') ? LEFT : DOWN;
      }
      else if(maze[playerY][playerX+1] == '1'){
        wall = RIGHT;
      }
      else{
        wall = DOWN;
      }
    }
    while(wall == DOWN){
      if(findExit(maze,playerX,playerY) == 1){
        exitFound = 1;
	break;
      }
      if(maze[playerY][playerX-1] == '0'){
        --playerX;
        mazePath[playerY][playerX] = 'X';
        printMaze(maze,mazePath);
        wall = (maze[playerY+1][playerX] == '1') ? DOWN : RIGHT;
      }
      else if(maze[playerY-1][playerX] == '1'){
        wall = UP;
      }
      else{
        wall = LEFT;
      }
    }
    while(wall == RIGHT){
      if(findExit(maze,playerX,playerY) == 1){
        exitFound = 1;
	break;
      }
      if(maze[playerY+1][playerX] == '0'){
        ++playerY;
        mazePath[playerY][playerX] = 'X';
        printMaze(maze,mazePath);
       wall = (maze[playerY][playerX+1] == '1') ? RIGHT : UP;
      }
      else if(maze[playerY][playerX-1] == '1'){
        wall = LEFT;
      }
      else{
        wall = DOWN;
      }
    }
  }
  printf("\nExit found\n");
  return 0;
}

void printMaze(char maze[][WIDTH], char mazePath[][WIDTH])
{
  static int step = 1;
  printf("\nStep %d:\n",step);
  ++step;
  for(int i = 0; i < HEIGHT; i++){
    for(int j = 0; j < WIDTH; j++){
      if(mazePath[i][j] == 'X')
        printf("%c ", 'X');
      else
        printf("%c ", maze[i][j]);
    }
    putchar('\n');
  }
}

int findExit(char maze[][WIDTH], int x, int y)
{
  if(x+1 == WIDTH || x-1 == -1)
    return 1;
  if(y+1 == HEIGHT || y-1 == -1)
    return 1;
  return 0;
}
