/* Вариант 1 Задание 2 */

#include<stdio.h>

#define A_DIMENSION_SIZE 5
#define B_DIMENSION_SIZE 3
#define A_SIZE A_DIMENSION_SIZE * A_DIMENSION_SIZE
#define B_SIZE B_DIMENSION_SIZE * B_DIMENSION_SIZE

void TransCol(int*, int);
void printArr(int*, int);

int main()
{
  int a[A_SIZE];
  int b[B_SIZE] = { 5, 3, 7, -1, -3, -5, 4, 7, 9 };
  for(int i = 0; i < A_SIZE; i++){
    printf("Please enter value for a[%d][%d]: ", i/A_DIMENSION_SIZE, i%A_DIMENSION_SIZE);
    if(scanf("%d", &a[i]) <= 0){
      while(getchar() != '\n')
        ;
      printf("Failed to read value for a[%d][%d], using 0\n", i/A_DIMENSION_SIZE, i%A_DIMENSION_SIZE);
      a[i] = 0;
    }
  }
  TransCol(a, A_DIMENSION_SIZE);
  printArr(a, A_DIMENSION_SIZE);
  TransCol(b, B_DIMENSION_SIZE);
  printArr(b, B_DIMENSION_SIZE);
  return 0;
}

void TransCol(int *matrix, int dimension)
{
  int i,j,t;
  i = 0;
  j = dimension-1;
  while(i < dimension){
    t = matrix[i];
    matrix[i] = matrix[j];
    matrix[j] = t;
    i++;
    j+=dimension;
  }
}

void printArr(int *matrix, int dimension)
{
  int size = dimension * dimension;
  for(int i = 0; i < size; i++){
    if(i % dimension == 0)
      putchar('\n');
    printf("%5d ", matrix[i]);
  }
  printf("\n\n");
}
