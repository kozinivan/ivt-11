/* Вариант 1 Задание 1 */

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define SIZE 10

int replaceInt(char *);

int main()
{
  srand(time(NULL));
  char st[SIZE];
  for(int i = 0; i < SIZE-1; i++){
    st[i] = rand() % 95 + 32;
  }
  st[SIZE-1] = '\0';
  printf("\n%-17s%s\n","Source string: ", st);
  int numbersFound = replaceInt(st);
  printf("%-17s%s\n\nTotal numbers found: %d\n\n", "Modified string: ", st, numbersFound);
  return 0;

}

int replaceInt(char *s)
{
  int nums = 0;
  while(*s){
    if(*s >= '0' && *s <= '9'){
      *s = '#';
      ++nums;
    }
    ++s;
  }
  return nums;
}
