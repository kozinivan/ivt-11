#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define TRACK_D 70
#define RABBIT 1
#define TURTLE 2
#define BOTH 3
#define T_FAST 4
#define T_SLIDE 5
#define T_SLOW  6
#define R_SLEEP 7
#define R_BJUMP 8
#define R_BSLIDE 9
#define R_LJUMP 10
#define R_LSLIDE 11

int roll(int);
int move(int*,int**,int,int);

int main()
{
  srand(time(NULL));
  int track[TRACK_D] = { 0 };
  int *rabbit = track;
  int *turtle = track;
  int winner = 0;
  int move_type;
  while(!winner){
    move_type = roll(TURTLE);
    switch(move_type){
      case T_FAST:
       winner = move(track, &turtle,TURTLE,3); 
       break;
      case T_SLIDE:
       winner = move(track, &turtle, TURTLE,-6);
       break;
      case T_SLOW:
       winner = move(track, &turtle, TURTLE,1);
       break;
    }
    move_type = roll(RABBIT);
    switch(move_type){
     case R_SLEEP:
       break;
     case R_BJUMP:
       winner = move(track, &rabbit, RABBIT, 9);
       break;
     case R_BSLIDE:
       winner = move(track, &rabbit, RABBIT, -12);
       break;
     case R_LJUMP:
       winner = move(track, &rabbit, RABBIT, 1);
       break;
    case R_LSLIDE:
       winner = move(track, &rabbit, RABBIT, -2);
       break;
    }
  }
  printf("%s\n", (winner == RABBIT) ? "rabbit" : "turtle");
  return 0;
}

int roll(int character)
{
  int move = rand() % 10;
  if(character == TURTLE){
    if(move >= 0 && move < 5)
      return T_FAST;
    else if(move >= 5 && move < 7)
      return T_SLIDE;
    else
      return T_SLOW;
  }
  else{
    if(move == 0 || move == 1)
     return R_SLEEP;
    else if(move == 2 || move == 3)
     return R_BJUMP;
    else if(move == 4)
     return R_BSLIDE;
    else if(move >= 5 && move < 8)
     return R_LJUMP;
    else
     return R_LSLIDE;
  }
  return -1;
}

int move(int *track, int **place, int character, int move)
{
  if(**place == BOTH)
    **place = (character == TURTLE)? RABBIT : TURTLE;
  else
    **place = 0;
  if(*place+move < track+TRACK_D && *place+move >= track){
    *place+=move;
  }
  else if(*place+move > track+TRACK_D)
    return character;
  else
    *place = track;
  if(**place != 0){
    **place = character;
  }
  else
   **place = BOTH;
  return 0;
}
