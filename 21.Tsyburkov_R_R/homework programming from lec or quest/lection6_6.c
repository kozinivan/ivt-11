#include<stdio.h>
#include<stdlib.h>
#define SIZE 9

int main()
{
int a[SIZE];
srand(time(NULL));

putchar('\n');
for (int i = 0; i <= SIZE; i++)
{
a[i] = (rand() % 100) + 1;
printf("a[%d] = %d\n", i, a[i]);
}
printf("\na[0] = %d\n", a[0]); // firt element
for (int i = 1; i < SIZE; i++)
{
a[i] = (a[i - 1] + a[i + 1]) / 2; // symma/2 element
printf("\na[%d] = %d\n", i, a[i]);
}
printf("\na[%d] = %d\n", SIZE, a[SIZE]); // last element
return 0; 
}
