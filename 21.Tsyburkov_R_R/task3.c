#include<stdio.h>
#include<math.h>

double fun(double, double);
 double fun(double x, double y)
 {
	return sqrt((x+1.0)/(y+3.0/2.0));
 }

int main()
{
	double x,y;
	printf("|   X   |   Y   |   F(x,y)  |\n");
	for(x = 0.0;x<=1.0; x+=0.1) {
		printf("\n");
		for(y = -3.0; y<=3.0; y+=0.5)
		{
			if(y == (-3.0/2.0)) //исключает деление на 0
			{
				printf("error no value");
				putchar('\n');
				continue;
			}
			printf("| %.1f | %.1f | %.3f |\n",x,y,fun(x,y));
		}
	}
	return 0;
}

 