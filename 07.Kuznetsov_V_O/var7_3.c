#include<stdio.h>
#include<math.h>
#include<stdlib.h>

double S(double);
unsigned long fact(unsigned long);

int main()
{
	double x;
	printf("Введите число \"Х\"\n");
	scanf("%lf", &x);
	printf("S равно: %lf\n", S(x));
	return 0;
}

double S(double x)
{
	int k = 1;
	int stp = 1;
	double rslt = 0;
	double a;
	while(stp)
	{
		a = pow(-1, k) * (pow(x, 2 * k + 1) / fact(2 * k + 1));
		//printf("%lf\n", a);
		if(fabs(a) <= 0.001)
			stp = 0;
		rslt += a;
		++k;
	}
	return rslt;
}

unsigned long fact(unsigned long f)
{
	if(f == 0)
		return 1;
	return(f * fact(f - 1));
}
