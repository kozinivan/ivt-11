#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
/*Контрольная 3 вариант 7, Кузнецов*/
int func(double *x, double *y, double *rst)
{
    *rst=((*x+*y-2)/(pow(*x,2)+pow(*y,2)-4))*acos(*x+*y);
    return 0;
}

int main()
{
    double x=-1,y=-1,rst=0;
    for (;x<=1;x+=0.25,y=-1)
    {
        for (; y < 1; y+=0.1)
        {
            func(&x,&y,&rst);
            if (rst!=rst)
            {
                break;
            }
            else
            {
                printf("Current values is %.3lf\t\tx: %.3lf\ty: %.3lf\n", rst, x, y);
            }
        }
    }
    return 0;
}
