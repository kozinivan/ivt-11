#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define SIZEA 4
#define SIZEB 3

int sum(int[]);

int main()
{
	int B[SIZEA];
	int A[SIZEA][SIZEB];
	srand(time(NULL));
	printf("Massive A is:\n");
	for(int i=0;i<SIZEA;i++)
	{
		for(int j=0;j<SIZEB;j++)
		{
			A[i][j]=rand()%10;
			printf("%d ",A[i][j]);
		}
		printf("\n");
	}
	printf("Massive B is:\n");
	for(int i=0;i<SIZEA;i++)
		B[i] = sum(*(A+i));
	for(int i=0;i<SIZEA;i++)
	{
		printf("%d ", B[i]);
		if(i==SIZEA-1)
			printf("\n");
	}	
	return 0;
}

int sum(int B[])
{
	int s=0;
	for(int i=0;i<SIZEB;i++)
		s+=B[i];
	return s;
}