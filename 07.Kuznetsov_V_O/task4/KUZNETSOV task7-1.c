#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define SIZE 10


double cntr_p(double[]);
double cntr_d(double[]);
double cntr_z(double[]);

double main()
{
	double array[SIZE];
	srand(time(NULL));
	for(int i=0;i<SIZE;i++)
	{
		array[i]=rand()%10;
		if(rand()%2==0)
			array[i]*=-1;
		printf("%.3lf ",array[i]);
		if(i==SIZE-1)
			printf("\n");
	}
	cntr_p(array);
	cntr_d(array);
	cntr_z(array);
	return 0;
}

double cntr_p(double array[SIZE])
{
	int c, j;
	for(c=0, j=0;j<SIZE;j++)
	{
		if(array[j]>0)
			c++;
		if(j==SIZE-1)
			printf("count of bigger than zero is %d\n",c);
	}
}

double cntr_d(double array[SIZE])
{
	int c, j;
	for(c=0, j=0;j<SIZE;j++)
	{
		if(array[j]<0)
			c++;
		if(j==SIZE-1)
			printf("count of smaller than zero is %d\n",c);
	}
}

double cntr_z(double array[SIZE])
{
	int c, j;
	for(c=0, j=0;j<SIZE;j++)
	{
		if(array[j]==0)
			c++;
		if(j==SIZE-1)
			printf("count of equal to zero is %d\n",c);
	}
}