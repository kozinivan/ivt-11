#include <stdio.h> //Савинов К.И. Контрольная 3 Вариант 18
#include <stdlib.h> 
#include <math.h> 
float otr_fync(double, double); 
float pol_fync(double, double); 
int main() 
{ 
double a,b; 
for (a = 1.0, b = -2.0; a <= 2.1, b <= 2.0; a+=0.1, b+=0.5) 
{ 

if (b < 0) 
printf("При x = %.3f и y = %.3f , Ответ : %.3f\n", a, b, otr_fync(a,b)); 
if (b > 0) 
printf("При x = %.3f и y = %.3f , Ответ : %.3f\n", a, b, pol_fync(a,b)); 
} 
return 0; 
} 
float otr_fync(double x, double y){ 
return (x*log(x)+y*log10(y * (-1)))/(powl(x,3)-powl(y,3)); 
} 
float pol_fync(double x, double y){ 
return (x*log(x)+y*log10(y))/(powl(x,3)-powl(y,3)); 
}