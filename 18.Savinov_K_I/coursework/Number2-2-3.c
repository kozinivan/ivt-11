   //Подключаем библиотеки
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h>
    #include <time.h>
    long double func(int c, int d, long double x) { //объявление функции уравнения
     long double result; //переменная являющаяся результатом этого уравнения
     result = log2(c*x) - d; //Присвоение значения формулы
     return result;  //Возвращение результата
    }
  long double func2(int c, int d, long double x) { //объявление производной функции
    long double result;  //переменная являющаяся результатом этого уравнения
    result = c*(1/(d*x*log(2))); //Присвоение значения функции
    return result;  //Возвращение результата
    }
    int main() {
    double x,a,b,j; //Объявление переменных
    int i=0, c,d;  //Объявление переменных
    srand(time(NULL));//генерация случайных чисел
    a=rand()%500;//генерация случайного числа a
    a/=100;
    do {
    c=rand()%5+1;//генерация случайного числа c
    d=rand()%5+1;//генерация случайного числа d
    } while(c==0 || d==0);
    if(c>=0 && c!=1) //если с>=0 и с не равно 1 ,выполнить
    printf("уравнение: log2(%dx)-%d=0\n",c, d);//это выражение
    else printf("уравнение: log2(x)-%d=0\n", fabs(d));//иначе ,это
    printf("x0: %.2f\n",a);  //выводим значение x0
    printf("Введите точность: \n"); 
    scanf("%lf", &j); //ввод точности с клавиатуры
    do {
    x=a-func(c,d,a)/func2(c,d,a); //ввод формулы нахождения т.пересечения касательной с осью абсцисс
    a=x;  //Присваеваем переменной a значение x 
    i++; //Увеличиваем кол-во множителей на 1
    } while(fabs(func(c,d,x))>j); //условие цикла
    printf("Кол-во итераций: %d, результат: %.10Lf\n", i, func(c,d,x));
    return 0;//завершение программы
    }

