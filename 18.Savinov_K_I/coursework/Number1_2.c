//подключаем библиотеки
#include<stdio.h>
#include<stdlib.h>
#include<math.h> //подключаем математическую библиотеку для дальнейшего вычисления выражения
double fctr(double);
double fctr(double n)
{
if (n==1.0)
/* условие, при котором программа
заканчивает свою работу, когда n == 1 и возвращает выч.значение */
return 1;
else return n * fctr(n - 2.0); /* работа рекурсивной функции */
}
int main()
{
double x, eps;
printf("Введите x:");
scanf("%lf",&x);
printf("Введите eps-точность вычислении:");
scanf("%lf",&eps);
double xn;//тут храним N-ый член ряда
double n=3.0;
double s = 0;//тут храним значение суммы
int count = 0; /* переменная для подсчета кол-во членов ряда */
xn = x;//начальное значение xn
while (fabs(xn) > eps)
{
s += xn;//суммируем член ряда
count++;
n+=2;
xn=2*count+1/fctr(n-2.0)*pow(x,2*n);
printf("%lf\n членов ряда:%d\n",s,count);
}
return s;//Получившаяся сумма
}