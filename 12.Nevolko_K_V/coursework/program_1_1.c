#include <stdio.h> //подключение стандартной библиотеки
#include <stdlib.h>
#include <math.h> //подключенте математической библиотеки
#define turbo 0.00001

void T1(double (*Meduka)(double,double,double) ,double ,double ,double ,double); //объявление прототипов функций
double one(double,double,double);
double two(double,double,double);
double three(double,double,double);
double four(double,double,double);
double five(double,double,double);
double six(double,double,double);

void T1(double (*Meduka)(double,double,double) ,double x, double EPS, double c, double d) //обявление функции с алгоритмом вычисления
{
	int i=0;
	double pr, x1=0, x2=1; //объявление переменных
	while(fabs(x2-x1)>EPS) //объявление цикла while
	{
		i++;
		pr=(Meduka(x+turbo,c,d)-Meduka(x-turbo,c,d))/((x+turbo)-(x-turbo));  // алгоритм вычисления корня функции методом касательных
		x1=x-Meduka(x,c,d)/pr;	
		x2=x;
		x=x1;
		
		if(Meduka(x1-EPS,c,d)*Meduka(x1+EPS,c,d)>0)
			EPS=EPS*0.1;
		
		if(i>=200)
			EPS=1000;
	}
	printf("Колличество итераций: %d\n",i);
	printf("Х равен - %lf\n",x1); //вывод на экран результатов работы функции	
}

double one(double x, double c, double d) //обявление функций с уравнением для функции T1
{
	return sin(c*x)-d; //возвращение значеия функции
}

double two(double x, double c, double d) 
{
	return exp(c*x)-d;
}

double three(double x, double c, double d)
{
	return pow(x,5)+c*pow(x,2)-d;
}

double four(double x, double c, double d)
{
	return log(c*x)-d;
}

double five(double x, double c, double d)
{
	return sqrt(c*x)-d;
}

double six(double x, double c, double d)
{
	return (pow(x,3)-d)/(sqrt(x)*c);
}

int main() //обявление главной функции
{
	double a, EPS, c, d; //объявление переменных
	
	//ввод значения с клавиатуры
	printf("Введите любую точку a\n");
	scanf("%lf",&a);
	printf("Введите значеия c и d\n");
	scanf("%lf%lf",&c,&d);
	printf("Задайте точность\n");
	scanf("%lf",&EPS);
	printf("\n\n");
	
	T1(one,a,EPS,c,d);  //вызов функции с алгоритмом вычисления от функций с первым уравнением
	printf("\n");
	T1(two,a,EPS,c,d);  
	printf("\n");
	T1(three,a,EPS,c,d);
	printf("\n");
	T1(four,a,EPS,c,d);
	printf("\n");
	T1(five,a,EPS,c,d);
	printf("\n");
	T1(six,a,EPS,c,d);
	
	return 0;
}