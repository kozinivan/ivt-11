#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 12

void T1(char [][SIZE]);  //объявление прототипов функций
void T2(char [][SIZE]);
int T3(char [][SIZE], int, int);
void T4(char [][SIZE]);
int T5(char [][SIZE], int, int);
void ras(char [][SIZE]);
void one(char [][SIZE]);
void one1(char [][SIZE]);
void two(char [][SIZE]);
void three(char [][SIZE]);
void four(char [][SIZE]);
void botras(char [][SIZE]);
void botras1(char [][SIZE]);
void bossras1(char [][SIZE]);
void bossras2(char [][SIZE]);
void bossras3(char [][SIZE]);
int game(char [][SIZE], char [][SIZE], char [][SIZE]);

void T1(char x[SIZE][SIZE]) //Фукция заполнения массива "боевого поля"
{
	for(int i=0;i<SIZE;i++)
		for(int j=0;j<SIZE;j++)
			x[i][j]='*';
		
	for(int i=1;i<SIZE-1;i++)
		for(int j=1;j<SIZE-1;j++)
			x[i][j]=' ';
}

void T2(char x[SIZE][SIZE]) //Фукция вывода на экран массива "боевого поля"
{
	printf("     1 2 3 4 5 6 7 8 9 10\n");
	
	printf("  |");
	
	for(int i=0;i<SIZE;i++)
	{
		if(i<=9 && i>0)
			printf(" %d|",i);
		if (i>9 && i<11)
			printf("%d|",i);
		if(i==11)
			printf("  |");
		for(int j=0;j<SIZE;j++)
		{
		printf("%c ",x[i][j]);
		}
		printf("\n");
	}
		printf("\n\n");
}

int T3(char x[SIZE][SIZE], int a, int b) //Фукция поиска разрешённого места для расстаноыки коробля
{
	int z;
			if(x[a][b]==' ')
			{
				if(x[a+1][b-1]!='@' && x[a+1][b+1]!='@' && x[a-1][b-1]!='@' && x[a-1][b+1]!='@' && x[a-1][b]!='@' && x[a][b+1]!='@' && x[a+1][b]!='@' && x[a][b-1]!='@')
					z=1;
				else
				{
					z=0;
				}
			}
			else
			{
				z=0;
			}
			return z;
}

void T4(char x[SIZE][SIZE]) //Фукция определения потоплённого коробля путём обведения его сигментов
{
	for(int a=1;a<SIZE-1;a++)
		for(int b=1;b<SIZE-1;b++)
		{
	if(x[a][b]=='X')
	{
		if(x[a+1][b-1]!='@' && x[a+1][b+1]!='@' && x[a-1][b-1]!='@' && x[a-1][b+1]!='@' && x[a-1][b]!='@' && x[a][b+1]!='@' && x[a+1][b]!='@' && x[a][b-1]!='@')
		{
			for(int i=a-1;i<=a+1;i++)
				for(int j=b-1;j<=b+1;j++)
				{
					if(x[i][j]!='X')
					x[i][j]='*';
				}
		}
	}
		}
}

int T5(char x[SIZE][SIZE], int a, int b) //функция расширения возможностей ИИ путём добавления ему массовой атаки
{
	int n=1;
		for(int i=a-1;i<=a+1;i++)
			for(int j=b-1;j<=b+1;j++)
			{
				if(((i==a-1) && (j==b)) || ((i==a+1) && (j==b)) || ((i==a) && (j==b-1)) || ((i==a) && (j==b+1)) || (i==a && j==b))
				{
				if(x[i][j]=='@')
				{
					n++;
					x[i][j]='X';
				}
				if(x[i][j]==' ')
					x[i][j]='*';
				}
			}
	return n;
}

void one(char x[SIZE][SIZE]) //функция расположения однопалубных кораблей игрока
{
	int a, b, x1=4;
	while(x1>0){
	printf("Задайте координаты расположения этого однопалубного тазика\n");
	printf("По горизонтали X: ");scanf("%d",&a);
	printf("По вертикали Y: ");scanf("%d",&b);
	printf("\n");
	if((a>10) || (b>10) || (a<1) || (b<1)){
		printf("Вы всё сломали\n\n");}
	else{
		if(T3(x,b,a)!=0)
		{
		x[b][a]='@';
		x1--;
		}
		else
			printf("Некорректные значения\n");
		T2(x);}
	}
}

void one1(char x[SIZE][SIZE]) //функция расположения однопалубных кораблей игрока для специального режима
{
	int a, b, x1=10;
	while(x1>0){
	printf("Задайте координаты расположения этого однопалубного тазика\n");
	printf("По горизонтали X: ");scanf("%d",&a);
	printf("По вертикали Y: ");scanf("%d",&b);
	printf("\n");
	if((a>10) || (b>10) || (a<1) || (b<1)){
		printf("Вы всё сломали\n\n");}
	else{
		if(T3(x,b,a)!=0)
		{
		x[b][a]='@';
		x1--;
		}
		else
			printf("Некорректные значения\n");
		T2(x);}
	}
}

void two(char x[SIZE][SIZE]) //функция расположения двухпалубных кораблей игрока
{
	int a, b, n, x2=3;
	while(x2>0){
	printf("Как вы хотите расположить кораблик:\n1-Горизонтально\n2-Вертикально\n\n");
	scanf("%d",&n);
	switch(n)
	{
		case 1:
			printf("Задайте координаты расположения этого двухпалубного тазика-значение X не больше 9\n");
			printf("По горизонтали X: ");scanf("%d",&a);
			printf("По вертикали Y: ");scanf("%d",&b);
			printf("\n");
			
				if((a>9) || (b>10) || (a<1) || (b<1)){
					printf("Вы всё сломали\n\n");}
				else{
					if(T3(x,b,a)!=0 && T3(x,b,a+1)!=0)
					{
					x[b][a]='@';
					x[b][a+1]='@';
					x2--;
					}
					else
						printf("Некорректные значения\n");
					T2(x);}
			break;
		case 2:
			printf("Задайте координаты расположения этого двухпалубного тазика-значение Y не меньше 2\n");
			printf("По горизонтали X: ");scanf("%d",&a);
			printf("По вертикали Y: ");scanf("%d",&b);
			printf("\n");
			
				if((a>10) || (b>10) || (a<1) || (b<2)){
					printf("Вы всё сломали\n\n");}
				else{
					if(T3(x,b,a)!=0 && T3(x,b-1,a)!=0)
					{
					x[b][a]='@';
					x[b-1][a]='@';
					x2--;
					}
					else
						printf("Некорректные значения\n");
					T2(x);}
			break;
		default:
			printf("Нет такой буквы, цифры не подходят\n");
	}}
}

void three(char x[SIZE][SIZE]) //функция расположения трёхпалубных кораблей игрока
{
	int a, b, n, x3=2;
	while(x3>0){
	printf("Как вы хотите расположить кораблик:\n1-Горизонтально\n2-Вертикально\n\n");
	scanf("%d",&n);
	switch(n)
	{
		case 1:
			printf("Задайте координаты расположения этого короля моря-значение X не больше 8\n");
			printf("По горизонтали X: ");scanf("%d",&a);
			printf("По вертикали Y: ");scanf("%d",&b);
			printf("\n");
			
				if((a>8) || (b>10) || (a<1) || (b<1)){
					printf("Вы всё сломали\n\n");}
				else{
					if(T3(x,b,a)!=0 && T3(x,b,a+2)!=0)
					{
					x[b][a]='@';
					x[b][a+1]='@';
					x[b][a+2]='@';
					x3--;
					}
					else
						printf("Некорректные значения\n");
					T2(x);}
			break;
		case 2:
			printf("Задайте координаты расположения этого короля моря-значение Y не меньше 3\n");
			printf("По горизонтали X: ");scanf("%d",&a);
			printf("По вертикали Y: ");scanf("%d",&b);
			printf("\n");
			
				if((a>10) || (b>10) || (a<1) || (b<3)){
					printf("Вы всё сломали\n\n");}
				else{
					if(T3(x,b,a)!=0 && T3(x,b-2,a)!=0)
					{
					x[b][a]='@';
					x[b-1][a]='@';
					x[b-2][a]='@';
					x3--;
					}
					else
						printf("Некорректные значения\n");
					T2(x);}
			break;
		default:
			printf("Нет такой буквы, цифры не подходят\n");
	}}
}

void four(char x[SIZE][SIZE]) //функция расположения четырёхпалубных кораблей игрока
{
	int a, b, n, x4=1;
	while(x4>0){
	printf("Как вы хотите расположить кораблик:\n1-Горизонтально\n2-Вертикально\n\n");
	scanf("%d",&n);
	switch(n)
	{
		case 1:
			printf("Задайте координаты расположения этого Левиафана-значение X не больше 7\n");
			printf("По горизонтали X: ");scanf("%d",&a);
			printf("По вертикали Y: ");scanf("%d",&b);
			printf("\n");
			
				if((a>7) || (b>10) || (a<1) || (b<1)){
					printf("Вы всё сломали\n\n");}
				else{
					if(T3(x,b,a)!=0 && T3(x,b,a+3)!=0)
					{
					x[b][a]='@';
					x[b][a+1]='@';
					x[b][a+2]='@';
					x[b][a+3]='@';
					x4--;
					}
					else
						printf("Некорректные значения\n");
					T2(x);}
			break;
		case 2:
			printf("Задайте координаты расположения этого Левиафана-значение Y не меньше 4\n");
			printf("По горизонтали X: ");scanf("%d",&a);
			printf("По вертикали Y: ");scanf("%d",&b);
			printf("\n");
			
				if((a>10) || (b>10) || (a<1) || (b<4)){
					printf("Вы всё сломали\n\n");}
				else{
					if(T3(x,b,a)!=0 && T3(x,b-3,a)!=0)
					{
					x[b][a]='@';
					x[b-1][a]='@';
					x[b-2][a]='@';
					x[b-3][a]='@';
					x4--;
					}
					else
						printf("Некорректные значения\n");
					T2(x);}
			break;
		default:
			printf("Нет такой буквы, цифры не подходят\n");
	}}	
}

void ras(char a[SIZE][SIZE]) //функция выбора расстановки кораблей игрока
{
	int n, i=4;
	int x1=1, x2=1, x3=1, x4=1;
	
	while(i!=0){
	printf("Расставьте свои корабли\nВыбирете кораблики:\n1-Торпедный катер, одна клетка\n2-Эсминец, две клетки\n3-Крейсер, три клетки\n4-Линкор, четыре клетки\n\n");
	scanf("%d",&n);
	switch(n)
	{
		case 1:
		if(x1!=0){
			one(a);x1=0;i--;}
		else
			printf("У вас кончились караблики этого вида\n");
			break;
		case 2:
		if(x2!=0){
			two(a);x2=0;i--;}
		else
			printf("У вас кончились караблики этого вида\n");
			break;
		case 3:
		if(x3!=0){
			three(a);x3=0;i--;}
		else
			printf("У вас кончились караблики этого вида\n");
			break;
		case 4:
		if(x4!=0){
			four(a);x4=0;i--;}
		else
			printf("У вас кончились караблики этого вида\n");
			break;
		default:
			printf("Нет такой буквы, цифры не подходят\n");
			
	}}
}

void botras(char b[SIZE][SIZE]) //функция расстаноыки короблей ИИ для режима "Классический"
{
	int i=4, x1;
	srand(time(NULL));
	while(i!=0)
	{
	
	x1=1;
	
	switch(i)
	{
	
		case 1:
		if(x1!=0){
			
			int c, d, y1=4;
			while(y1>0){
				
			c=rand()%10+1;
			d=rand()&10+1;

				if((c<=10) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0)
					{
					b[d][c]='@';
					y1--;
					}		
				}
			}
		
		;x1=0;i--;}
			break;
			
			
		case 2:
		if(x1!=0){
				
	int c, d, n, y2=3;
	while(y2>0){
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=9) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+1)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					y2--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=2))
				{
					if(T3(b,d,c)!=0 && T3(b,d-1,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					y2--;
					}
				}
			break;
	}}
			
			x1=0;i--;}
			break;
			
			
		case 3:
		if(x1!=0){
			
	int c, d, n, y3=2;
	while(y3>0){

		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=8) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+2)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					b[d][c+2]='@';
					y3--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=3))
				{
					if(T3(b,d,c)!=0 && T3(b,d-2,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					b[d-2][c]='@';
					y3--;
					}
				}
			break;
	}}
			
			x1=0;i--;}
			break;
			
			
		case 4:
		if(x1!=0){
			
	int c, d, n, y4=1;
	while(y4>0){
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;		
		
	switch(n)
	{
		case 1:
			
				if((c<=7) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+3)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					b[d][c+2]='@';
					b[d][c+3]='@';
					y4--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=4))
				{
					if(T3(b,d,c)!=0 && T3(b,d-3,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					b[d-2][c]='@';
					b[d-3][c]='@';
					y4--;
					}
				}
			break;
	}}
			
			x1=0;i--;}
			break;
		}}
}

void botras1(char b[SIZE][SIZE]) //функция расстаноыки короблей ИИ для режима "Только однопалубные корабли"
{
	srand(time(NULL));
	
	int c, d, y1=10;
		while(y1>0){
				
			c=rand()%10+1;
			d=rand()&10+1;

				if((c<=10) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0)
					{
					b[d][c]='@';
					y1--;
					}		
				}
			}
		

}

void bossras1(char b[SIZE][SIZE]) //функция расстаноыки короблей ИИ для режима "Испытание"
{
	srand(time(NULL));
	
	int c, d, n, y1=5, y2=4, y4=2;
		while(y1>0)
		{
			c=rand()%10+1;
			d=rand()&10+1;

				if((c<=10) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0)
					{
					b[d][c]='@';
					y1--;
					}		
				}
		}
		
		while(y2>0)
		{
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=9) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+1)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					y2--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=2))
				{
					if(T3(b,d,c)!=0 && T3(b,d-1,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					y2--;
					}
				}
			break;
	}	
		}
		
		while(y4>0)
		{
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;		
		
	switch(n)
	{
		case 1:
			
				if((c<=7) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+3)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					b[d][c+2]='@';
					b[d][c+3]='@';
					y4--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=4))
				{
					if(T3(b,d,c)!=0 && T3(b,d-3,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					b[d-2][c]='@';
					b[d-3][c]='@';
					y4--;
					}
				}
			break;
	}
		}
			
}

void bossras2(char b[SIZE][SIZE]) //функция расстаноыки короблей ИИ для режима "Испытание"
{
	srand(time(NULL));
	
	int c, d, n, y1=6, y2=2, y3=1, y4=1;
	while(y1>0)
		{
			c=rand()%10+1;
			d=rand()&10+1;

				if((c<=10) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0)
					{
					b[d][c]='@';
					y1--;
					}		
				}
		}
		
		while(y2>0)
		{
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=9) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+1)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					y2--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=2))
				{
					if(T3(b,d,c)!=0 && T3(b,d-1,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					y2--;
					}
				}
			break;
	}	
		}
		
		while(y4>0)
		{
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;		
		
	switch(n)
	{
		case 1:
			
				if((c<=7) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+3)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					b[d][c+2]='@';
					b[d][c+3]='@';
					y4--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=4))
				{
					if(T3(b,d,c)!=0 && T3(b,d-3,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					b[d-2][c]='@';
					b[d-3][c]='@';
					y4--;
					}
				}
			break;
	}
		}
		
		while(y3>0)
		{

		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=8) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+2)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					b[d][c+2]='@';
					y3--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=3))
				{
					if(T3(b,d,c)!=0 && T3(b,d-2,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					b[d-2][c]='@';
					y3--;
					}
				}
			break;
	}
		}
}

void bossras3(char b[SIZE][SIZE]) //функция расстаноыки короблей ИИ для режима "Испытание"
{
	srand(time(NULL));
	
	int c, d, n, y1=6, y2=4, y3=1;
	while(y1>0)
		{
			c=rand()%10+1;
			d=rand()&10+1;

				if((c<=10) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0)
					{
					b[d][c]='@';
					y1--;
					}		
				}
		}
		
		while(y2>0)
		{
		
		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=9) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+1)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					y2--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=2))
				{
					if(T3(b,d,c)!=0 && T3(b,d-1,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					y2--;
					}
				}
			break;
	}	
		}
		
		while(y3>0)
		{

		c=rand()%10+1;
		d=rand()%10+1;
		n=rand()%2+1;
	
	switch(n)
	{
		case 1:
			
				if((c<=8) && (d<=10) && (c>=1) && (d>=1))
				{
					if(T3(b,d,c)!=0 && T3(b,d,c+2)!=0)
					{
					b[d][c]='@';
					b[d][c+1]='@';
					b[d][c+2]='@';
					y3--;
					}
				}
			break;
			
		case 2:
			
				if((c<=10) && (d<=10) && (c>=1) && (d>=3))
				{
					if(T3(b,d,c)!=0 && T3(b,d-2,c)!=0)
					{
					b[d][c]='@';
					b[d-1][c]='@';
					b[d-2][c]='@';
					y3--;
					}
				}
			break;
	}
		}
}

int game(char a[SIZE][SIZE], char b[SIZE][SIZE], char c[SIZE][SIZE]) //функция игрового процесса
{
	int k=1, x, y, rd, bilbo, begenc, bilbobegenc=1;
	srand(time(NULL));
	
	rd=rand()%2+1;
	
	while(bilbobegenc!=0)
	{
		bilbo=0;
		begenc=0;
		
		if(rd==1)
		{
			printf("Ваш ход, задайте координаты удара\n");
			printf("по горизонтали: ");scanf("%d",&x);
			printf("по вертикали: ");scanf("%d",&y);
			if(x<=10 && x>=1 && y<=10 && y>=1)
			{
				if((b[y][x]=='X') || (b[y][x])=='*'){
					rd=1;
				printf("Вы ударили в одну и туже точку\n\n");}
				if(b[y][x]=='@'){
					b[y][x]='X';
					c[y][x]='X';
					T4(b);
					
					for(int i=1;i<SIZE-1;i++)
						for(int j=1;j<SIZE-1;j++)
						{
							if(b[i][j]=='*')
								c[i][j]='*';
						}
						
				rd=1;}
				if(b[y][x]==' '){
					b[y][x]='*';
					c[y][x]='*';
				rd=2;}
			}
			else
				printf("\nНекоректное значение\n");
		}
		if(rd>1)
		{
			k++;
			printf("\nХод потивника\n");
			x=rand()%10+1;
			y=rand()%10+1;
				if((a[y][x]=='X') || (a[x][y])=='*'){
				rd=2;}
				if(a[y][x]=='@'){
					if(k%5==0)
						rd=T5(a,y,x);
					else
						a[y][x]='X';
				
					T4(a);
				rd=2;}
				if(a[y][x]==' '){
					if(k%5==0)
						rd=T5(a,y,x);
					else
					a[y][x]='*';
				
				T4(a);
				rd=1;}
		}
	
		printf("\nПоле ваших кораблей:\n");
		T2(a);
		printf("\n\nПоле противника\n");
		T2(c);
		if(x==17 && y==2){
		printf("\nПоле вражеских кораблей\n");
		T2(b);}
		
		for(int i=1;i<SIZE-1;i++)
			for(int j=1;j<SIZE-1;j++)
			{
				if(a[i][j]=='@')
					bilbo++;
				if(b[i][j]=='@')
					begenc++;
			}
			
			if(bilbo==0){
				bilbobegenc=bilbo;
				printf("\n\tВы проиграли, это конец\n\n");
			return bilbo;}
			if(begenc==0){
				bilbobegenc=begenc;
				printf("\n\tНа этот раз вы победили\n\n");
			return rd;}
}

}

int main() //главная функция
{
	int n, boss;
	char a[SIZE][SIZE], b[SIZE][SIZE], c[SIZE][SIZE];
	srand(time(NULL));
	printf("\n\n\tBattleship\n\n");
	printf("Выбирите режим игры:\n1)-Классический\n2)-Только однопалубные корабли\n3)-Испытание\n");
	scanf("%d",&n);
	switch(n)
	{
		case 1:
			T1(a);T1(b);T1(c);
			printf("Ваше поле растановки кораблей:\n");
			T2(a);
			printf("Поле противника:\n");
			T2(c);
			botras(b);
			ras(a);
			game(a,b,c);
		break;
		
		case 2:
			T1(a);T1(b);T1(c);
			printf("Ваше поле растановки кораблей:\n");
			T2(a);
			printf("Поле противника:\n");
			T2(c);
			botras1(b);
			printf("Расставьте свои корабли\n\n");
			one1(a);
			game(a,b,c);
		break;
		
		case 3:
		
			printf("Ваш первый противник в режиме Испытание - Ржавый Джек\n\n");
			printf("-Хо Хо, смотрите какую рыбку принесло течением на наши просторы!\nХватайте его парни!\n\n");
			T1(a);T1(b);T1(c);
			printf("Ваше поле растановки кораблей:\n");
			T2(a);
			printf("Поле противника:\n");
			T2(c);
			bossras1(b);
			printf("Расставьте свои корабли\n\n");
			four(a);
			four(a);
			one(a);
			boss=game(a,b,c);
			if(boss==0)
				break;
			
			printf("Ваш второй противник в режиме Испытание - Госпажа Удача\n\n");
			printf("-Да как ты посмел уничтожить флот моего верного помошника!\nИз всез орудий ОГОНЬ!!!\n\n");
			T1(a);T1(b);T1(c);
			printf("Ваше поле растановки кораблей:\n");
			T2(a);
			printf("Поле противника:\n");
			T2(c);
			bossras2(b);
			printf("Расставьте свои корабли\n\n");
			four(a);
			four(a);
			one(a);
			boss=game(a,b,c);
			if(boss==0)
				break;
			
			printf("Ваш последний противник в режиме Испытание - Морской Охотник\n\n");
			printf("-Я смотрю ты не промах, но к сожалению твои странствия окончены.\n\n");
			T1(a);T1(b);T1(c);
			printf("Ваше поле растановки кораблей:\n");
			T2(a);
			printf("Поле противника:\n");
			T2(c);
			bossras3(b);
			printf("Расставьте свои корабли\n\n");
			four(a);
			four(a);
			one(a);
			boss=game(a,b,c);
			if(boss==0)
				break;
			
		break;
		
		default:
			printf("Нет такой буквы\n");
	}

	return 0;
}