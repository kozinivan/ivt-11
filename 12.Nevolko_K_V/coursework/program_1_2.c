#include <stdio.h> //подключение стандартной библиотеки
#include <stdlib.h>
#include <math.h> //подключенте математической библиотеки

 //объявление прототипов функций
void T1(double (*Meduka)(double,double,double) ,double ,double ,double ,double ,double);
double one(double,double,double);
double two(double,double,double);
double three(double,double,double);
double four(double,double,double);
double five(double,double,double);
double six(double,double,double);


//обявление функции с алгоритмом вычисления
void T1(double (*Meduka)(double,double,double),double a,double b,double EPS,double c,double d)
{
	int i=0;
	double x; //объявление переменной
	if(Meduka(a,c,d) * Meduka(b,c,d) <= 0) //условие для выполнения последующих действий
	{
		while((b-a)>EPS) //объявление цикла while
		{
			i++;
			x=(a+b)/2;                   // алгоритм вычисления корня функции методом деления отрезка пополам
			if(Meduka(x,c,d)*Meduka(b,c,d)>0)
				b=x;
			else
				a=x;
		}
		printf("Колличество итераций: %d\n",i);
		printf("Х равен - %lf\n",x); //вывод на экран результатов работы функции
	}
	else
		printf("Введены не корректные значения\n"); //вывод на экран сообщения об ошибки
}

double one(double x, double c, double d) //обявление функций с уравнением для функции T1
{
	return sin(c*x)-d; //возвращение значеия функции
}

double two(double x, double c, double d)
{
	return exp(c*x)-d;
}

double three(double x, double c, double d)
{
	return pow(x,5)+c*pow(x,2)-d;
}

double four(double x, double c, double d)
{
	return log(c*x)-d;
}

double five(double x, double c, double d)
{
	return sqrt(c*x)-d;
}

double six(double x, double c, double d)
{
	return (pow(x,3)-d)/(sqrt(x)*c);
}

int main() //обявление главной функции
{
	double a, b, EPS, c, d; //объявление переменных
	
	//ввод значений с клавиатуры
	printf("Введите интервал a и b\n");
	scanf("%lf%lf",&a,&b);
	printf("Введите значеия c и d\n");
	scanf("%lf%lf",&c,&d);
	printf("Задайте точность\n");
	scanf("%lf",&EPS);
	printf("\n\n");
	
	T1(one,a,b,EPS,c,d); //вызов функции с алгоритмом вычисления от функций с первым уравнением
	printf("\n");
	T1(two,a,b,EPS,c,d);
	printf("\n");
	T1(three,a,b,EPS,c,d);
	printf("\n");
	T1(four,a,b,EPS,c,d);
	printf("\n");
	T1(five,a,b,EPS,c,d);
	printf("\n");
	T1(six,a,b,EPS,c,d);
	return 0;
}